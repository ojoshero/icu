import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Moutopouloue on 15/07/2016.
 */
public class IPBetSizeProcessor implements DataProcessor{
    String item;
    String seederAppkey = "QpEFSGClskcGVNbG";
    Map<String, IpInfo> ipMap = new HashMap<>();

    public String process(String[] data)  {


        //1: Date-Time, 16: Requested Action (P=Place, C=Cancel, etc), 5: appKey
        // 23: size, 7: accountID 6:sourceIPAddress
        if (!data[4].equals(seederAppkey)) {


            if (ipMap.get(data[5]) != null) {
                float newBetSize = ipMap.get(data[5]).getBetSize() + Float.parseFloat(data[22]);
                int newCount = ipMap.get(data[5]).getCount() + 1;
                ipMap.put(data[5], new IpInfo(newBetSize, newCount, data[0]));
            } else {
                ipMap.put(data[5], new IpInfo(Float.parseFloat(data[22]), 1, data[0]));
            }



            IPLocation ipLocation = new IPLocation();
            try {
                ipLocation = ipLocation.ipConverter(data[5]);
            } catch (Exception e) {
                e.printStackTrace();
            }
            MapPosition mapPosition = new MapPosition();
            MapPosition mapPositionEU = new MapPosition();
            mapPosition = mapPosition.changeScale(ipLocation);
            mapPositionEU = mapPositionEU.changeScaleEurope(ipLocation);

            item = data[0] + "," + data[22] + "," + data[6] + "," + mapPosition.getLatitude() + "," + mapPosition.getLongitude() + "," + mapPositionEU.getLatitude() + "," + mapPositionEU.getLongitude();
        }
        return item;
    }

    public List<String> getAllLines() {
        List<String> lines = new ArrayList<>();

        for (Map.Entry entry : ipMap.entrySet()) {

            String ip = (String) entry.getKey();
            IpInfo info = (IpInfo) entry.getValue();

            IPLocation ipLocation = new IPLocation();
            try {
                ipLocation = ipLocation.ipConverter(ip);
            } catch (Exception e) {
                e.printStackTrace();
            }

            String line = ipLocation.getLatitude() + "," + ipLocation.getLongitude() + ","
            + info.getBetSize() + "," + info.getCount() + "," + info.getDate();

            lines.add(line);

        }

        return lines;
    }

    public void clearMap() {
        ipMap.clear();
    }

    class IpInfo {
        private Float betSize;
        private int count;
        private String date;

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public IpInfo(Float betSize, int count, String date) {
            this.betSize = betSize;
            this.count = count;
            this.date = date;
        }

        public Float getBetSize() {
            return betSize;
        }

        public void setBetSize(Float betSize) {
            this.betSize = betSize;
    }

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }
    }


}
