import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ReadCSVFile {
    protected static IPBetSizeProcessor ipBetSizeProcessor;
    private PrintWriter writer;
    public static void main(String[] args) {

        ReadCSVFile obj = new ReadCSVFile();
        try {
            obj.run();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void run() throws Exception {

        String csvFile = "./data/logfile.csv";
        BufferedReader br;
        String line;
        String cvsSplitBy = ",";
        ipBetSizeProcessor = new IPBetSizeProcessor();
        Boolean timerStart;
        Date dateTimeStart=null,dateTime;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        int i=0;

        //open the csvfile
        br = new BufferedReader(new FileReader(csvFile));
        timerStart = false;

            try {

                while ((line = br.readLine()) != null) {

                    // use comma as separator
                    String[] data = line.split(cvsSplitBy);
                    if (!timerStart) {
                        dateTimeStart = simpleDateFormat.parse(data[0]);  //2011-06-02 12:19:11.046
                        timerStart = true;
                        writer=createNewCSVFileWriter(i);

                    }


                    dateTime = simpleDateFormat.parse(data[0]);
                    if (((dateTimeStart.getTime() - dateTime.getTime()) / 1000) <= 1) {
                        ipBetSizeProcessor.process(data);

                    } else {
                        List<String> lines = ipBetSizeProcessor.getAllLines();
                        ipBetSizeProcessor.clearMap();
                        ipBetSizeProcessor.process(data);
                        for (String entry : lines) {
                            writer.println(entry);
                        }
                        writer.close();
                        i++;
                        timerStart = false;
                    }

                }
                List<String> lines = ipBetSizeProcessor.getAllLines();
                ipBetSizeProcessor.clearMap();
                for (String entry : lines) {
                    writer.println(entry);
                }
                writer.close();


            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (br != null) {
                    try {
                        br.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

        writer.close();
        System.out.println("Done");
    }

    public PrintWriter createNewCSVFileWriter(int i) throws IOException {
        //open new file to write
        File f = new File("./data/newData"+i+".csv");

        f.createNewFile();
        return new PrintWriter("./data/newData"+ i +".csv", "UTF-8");


    }

}


