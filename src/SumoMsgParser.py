import json
import csv

from pprint import pprint

with open('../data/raw.json') as data_file:
	data = json.load(data_file)['messages']

with open('../data/logfile.csv', 'w', newline='') as csvFile:
	for eachMap in data:
		if eachMap['map']['actionresponse'] == 'SUCCESS' and eachMap['map']['result'] == 'SUCCESS' and eachMap['map']['action'] == 'P':
			raw = eachMap['map']['_raw']
			csvFile.write(raw + "\n")
