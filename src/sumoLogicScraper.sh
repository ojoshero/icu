#!/bin/bash

# Variables
ENDPOINT="https://api.eu.sumologic.com/api/v1"
# FROMTIME="2016-07-15T13:15:00"
# TOTIME="2016-07-15T13:25:00"
[[ ! -n "$1" ]] && FROMTIME="2016-07-18T13:15:00" || FROMTIME=$1
[[ ! -n "$2" ]] && TOTIME="2016-07-18T13:25:00" || TOTIME=$2

OFFSET="0"
LIMIT="10000"
DEST_PATH_OUTPUT_FILE="../data/raw.json"

# Create a search job from a JSON file.
RESULT=$(curl -X POST -H "Authorization: Basic c3VKNGxOakN2cTM2eWY6dVRqRUxEMjdENDRoa09peXluSnRvQWRDaWhzTkI2TzZzYmNMQ2xMb2hOb0RKc3FLMWVjNkZUZXRuQTBRYkJONw==" -H "Content-Type: application/json" -H "Accept: application/json" -d '{
  "query": "_sourceCategory=production_exchange-transactional_betting",
  "from": "'$FROMTIME'",
  "to": "'$TOTIME'",
  "timeZone": "GMT"
}' "$ENDPOINT/search/jobs")
echo Result: $RESULT
JOB_ID=$(echo $RESULT | perl -pe 's|.*"id":"(.*?)"[,}].*|\1|')
echo Search job created, id: $JOB_ID

echo "Sleeping for 5 secs"
sleep 5

# Wait until the search job is done.
STATE=""
until [ "$STATE" = "DONE GATHERING RESULTS" ]; do
  RESULT=$(curl -X GET -H "Authorization: Basic c3VKNGxOakN2cTM2eWY6dVRqRUxEMjdENDRoa09peXluSnRvQWRDaWhzTkI2TzZzYmNMQ2xMb2hOb0RKc3FLMWVjNkZUZXRuQTBRYkJONw==" -H "Accept: application/json" "$ENDPOINT/search/jobs/$JOB_ID")
  STATE=$(echo $RESULT | sed 's/.*"state":"\(.*\)"[,}].*/\1/')
  MESSAGES=$(echo $RESULT | perl -pe 's|.*"messageCount":(.*?)[,}].*|\1|')
  # echo Result: $RESULT
  echo Search job state: $STATE, message count: $MESSAGES
  sleep 5
done


RESULT=""

function getData {
  RESULT=$(curl -sS -H "Authorization: Basic c3VKNGxOakN2cTM2eWY6dVRqRUxEMjdENDRoa09peXluSnRvQWRDaWhzTkI2TzZzYmNMQ2xMb2hOb0RKc3FLMWVjNkZUZXRuQTBRYkJONw==" -H "Cache-Control: no-cache" -H "Accept: application/json" "$ENDPOINT/search/jobs/$JOB_ID/messages?offset=$OFFSET&limit=$LIMIT")
}

getData

# Keep retrying until we get the results. Sleep every 5 seconds.
while [[ $RESULT == *"Job ID is invalid"* ]]
do
  getData
  echo "Did not get data, trying again in 5 seconds..."
  sleep 5
done

echo "Succesfully retrieved the data!"
# echo Result: $RESULT
echo "Writing messages into $DEST_PATH_OUTPUT_FILE"
echo $RESULT > $DEST_PATH_OUTPUT_FILE
