/**
 * Created by Moutopouloue on 15/07/2016.
 */
public class MapPosition {

    private String latitude;
    private String longitude;
    private int width= 2092;
    private int height= 1146;

    public MapPosition(String latitude, String longitude){
        this.latitude = latitude;
        this.longitude = longitude;
    }
    public MapPosition(){

    }

    public String getLatitude(){
        return this.latitude;
    }

    public String getLongitude(){
        return this.longitude;
    }

    public MapPosition changeScale(IPLocation ipLocation){


        float latitude =  Float.parseFloat(ipLocation.getLatitude());
        float longitude = Float.parseFloat(ipLocation.getLongitude());

        String pixelLat;
        String pixelLon;

        if (latitude > 0) {
            if (longitude < 54) {
                pixelLon = Float.toString(map(longitude, -180, 180, -20, width-20));
                pixelLat =  Float.toString(map(latitude, 90, -90, 170, height+170));
            } else if (longitude > 54) {
                pixelLon = Float.toString(map(longitude, -180, 180, -85, width-85));
                pixelLat =  Float.toString(map(latitude, 90, -90, 160, height+160));
            } else {
                pixelLon = Float.toString(map(longitude, -180, 180, -70, width-70));
                pixelLat =  Float.toString(map(latitude, 90, -90, 153, height+153));
            }
        } else {
            if (longitude < 54) {
                pixelLon = Float.toString(map(longitude, -180, 180, -60, width-60));
                pixelLat =  Float.toString(map(latitude, 90, -90, 140, height+140));
            } else if (longitude > 54) {
                pixelLon = Float.toString(map(longitude, -180, 180, -85, width-85));
                pixelLat =  Float.toString(map(latitude, 90, -90, 147, height+147));
            } else {
                pixelLon = Float.toString(map(longitude, -180, 180, -71, width-71));
                pixelLat =  Float.toString(map(latitude, 90, -90, 140, height+140));
            }

        }



        return new MapPosition(pixelLat,pixelLon);

    }

    public MapPosition changeScaleEurope(IPLocation ipLocation){

        float latitude =  Float.parseFloat(ipLocation.getLatitude());
        float longitude = Float.parseFloat(ipLocation.getLongitude());

        String pixelLat;
        String pixelLon;

        if (latitude > 0) {
            if (longitude < 54) {
                pixelLon = "-100.0";
                pixelLat = "-100.0";

            } else if (longitude > 54) {
                pixelLon = "-100.0";
                pixelLat = "-100.0";
            } else {
                pixelLon = Float.toString(map(longitude, -34, 67, 0, width));
                pixelLat =  Float.toString(map(latitude, 71, 32, 0, height));
            }

        } else {
            pixelLon = "-100.0";
            pixelLat = "-100.0";
        }


        return new MapPosition(pixelLat,pixelLon);

    }

    private float map(float x, int a, int b, int c, int d){

        return ((d-c)*(x-a)/(b-a)+c);
    }
}
