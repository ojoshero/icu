/**
 * Created by Moutopouloue on 15/07/2016.
 */
public interface DataProcessor {

    String process(String[] data);
}
