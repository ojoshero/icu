import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class IPLocation {
    private final String USER_AGENT = "Mozilla/5.0";

    private String latitude;
    private String longitude;
    private BufferedReader is;
    private StringBuffer response;
    String lat, lon;

    public IPLocation() {

    }

    public IPLocation(String latitude, String longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getLatitude() {
        return this.latitude;
    }

    public String getLongitude() {
        return this.longitude;
    }

    public IPLocation ipConverter(String ip) throws Exception {
        try {
            URL obj = new URL("http://freegeoip.net/json/"+ip);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("GET");

            con.setRequestProperty("User-Agent", USER_AGENT);

            int responseCode = con.getResponseCode();
//            System.out.println("Response Code : " + responseCode);

            BufferedReader is = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = is.readLine()) != null) {
                response.append(inputLine);
            }
            is.close();

//            System.out.println(response.toString());
            JSONObject json = new JSONObject(response.toString());
            lat = json.getString("latitude");
            lon = json.getString("longitude");


        } catch (IOException e) {
            e.printStackTrace();
        }

        return new IPLocation(lat, lon);
    }
}
