PImage img;
int alpha = 0;
float delta = 4.25;
float x;
float y;
ArrayList<Float> xs = new ArrayList();
ArrayList<Float> ys = new ArrayList();
ArrayList<Float> ds = new ArrayList();
ArrayList<Float> cs = new ArrayList();

String[] rows;
Boolean europe = false;
int fileNumber = 4;
String info = "EXCHANGE BETTING ACTIVITY";

int lastTime = 0;
int legendX = 523;
int legendY = 510;

int hours = 16;
int minutes = 31;
int seconds = 0;

void setup() {
 size(1046,573);
 img = loadImage("data/map.jpg");
 img.resize(1046,573);
 manyDots();
 frameRate(60);
}

void draw() {
  image(img, 0, 0);
  info();
   for (int i = 0; i < xs.size(); i++) {
     color colour = sizeToColour(cs.get(i));
     fill(colour, alpha);
     stroke(0,0);
     int diameter = countToDiameter(ds.get(i));
     if (europe) {
           ellipse(xs.get(i), ys.get(i), diameter, diameter);
     } else {
           ellipse(xs.get(i), ys.get(i), diameter, diameter);
      }
   }
   glow();

}

void keyPressed() {
  if (keyCode == UP) {
    europe = true;
    img = loadImage("data/map2.jpg");
    img.resize(1046,573);
    info = "";
    legendX = 112;
    legendY = 510;
    manyDots();
  }
  if (keyCode == DOWN) {
    img = loadImage("data/map.jpg");
    img.resize(1046,573);
    europe = false;
    manyDots();
    legendX = width/2;
    legendY = 510;
    info = "EXCHANGE BETTING ACTIVITY";
  }
}

void glow() {
if (alpha < 0 || alpha > 255) {
   delta = -delta;
   if (alpha < 0) {
    manyDots();
   }
   if (alpha > 255) {
    seconds++;
   }
 }
 alpha += delta;
}

void manyDots() {
 xs.clear();
 ys.clear();
 PVector coords;
 rows = loadStrings("newData" + fileNumber + ".csv");
 for (String row : rows) {
   if (europe) {
     coords = getPixelsEurope(float(row.split(",")[1]), float(row.split(",")[0]));
   } else {
     coords = getPixels(float(row.split(",")[1]), float(row.split(",")[0]));
   }
   xs.add(coords.x);
   ys.add(coords.y);
   ds.add(float(row.split(",")[3]));
   cs.add(float(row.split(",")[2]));
 }
 String time = rows[0].split(",")[4].split(" ")[1];
 hours = int(time.split(":")[0]);
 minutes = int(time.split(":")[1]);
 seconds = int(time.split(":")[2]);

 fileNumber--;
 if (fileNumber == -1) {
   fileNumber = 4;
 }

}

PVector getPixels(float longitude, float latitude) {
  if (latitude > 0) {
    if (longitude < -54) {
          return new PVector(
            map(longitude, -180, 180, -10, width-10),
            map(latitude, 90, -90, 85, height+85)
          );
    } else if (longitude > 54) {
          return new PVector(
            map(longitude, -180, 180, -42, width-42),
            map(latitude, 90, -90, 80, height+80)
          );
    } else {
      return new PVector(
        map(longitude, -180, 180, -35, width-35),
        map(latitude, 90, -90, 76, height+76)
      );
    }
  } else {
      if (longitude < -54) {
          return new PVector(
            map(longitude, -180, 180, -30, width-30),
            map(latitude, 90, -90, 70, height+70)
          );
    } else if (longitude > 54) {
          return new PVector(
            map(longitude, -180, 180, -42, width-42),
            map(latitude, 90, -90, 73, height+73)
          );
    } else {
      return new PVector(
        map(longitude, -180, 180, -35, width-35),
        map(latitude, 90, -90, 70, height+70)
      );
    }
  }
}

PVector getPixelsEurope(float longitude, float latitude) {
  if (latitude > 0) {
    if (longitude < -54) {
          return new PVector(-100, -100);
    } else if (longitude > 54) {
          return new PVector(-100, -100);
    } else {
      return new PVector(
        map(longitude, -34, 67, 0, width),
        map(latitude, 71, 32, 0, height)
      );
    }
  } else {
    return new PVector(-100, -100);
  }
}

int countToDiameter(float count) {
  if (count < 3) {
    return 15;
  } else if (count <10) {
    return 20;
  } else {
    return 25;
  }
}

color sizeToColour(float size) {
  if (size < 5) {
    return color(204,255,152);
  } else if (size < 50) {
    return color(154,205,50);
  } else {
    return color(0,100,0);
  }
}

void info() {
  textAlign(CENTER);
  textSize(25);
  fill(0);
  text(info, width/2, 30);
  stroke(0);
  strokeWeight(10);
  fill(0,0,0,0);
  rect(0,0,width,height);
  legend();
  clock();
}

void legend() {
  rectMode(CENTER);
  strokeWeight(1);
  rect(legendX,legendY,200,80);
  rectMode(CORNER);
  textSize(8);
  fill(0);
  text("TOTAL BET SIZE", legendX - 50, legendY - 25);
  text("NUMBER OF BETS", legendX + 50, legendY - 25);
  noStroke();
  fill(204,255,152);
  ellipse(legendX - 80, legendY - 12, 15, 15);
  fill(154,205,50);
  ellipse(legendX - 80, legendY + 7, 15, 15);
  fill(0, 100, 0);
  ellipse(legendX - 80, legendY + 26, 15, 15);
  stroke(1);
  noFill();
  ellipse(legendX + 80, legendY - 12, 15, 15);
  ellipse(legendX + 40, legendY + 7, 20, 20);
  ellipse(legendX + 80, legendY + 26, 25, 25);
  fill(0);
  text("< £5",legendX - 55, legendY - 10);
  text("< £50",legendX - 55, legendY + 8);
  text("> £50",legendX - 55, legendY + 28);
  text("< 3 bets",legendX + 45, legendY - 10);
  text("3<x<10",legendX + 70, legendY + 8);
  text("> 10 bets",legendX + 45, legendY + 28);
}

void clock() {
  if (seconds == 60) {
    seconds = 0;
    minutes++;
  }
  if (minutes == 60) {
    minutes = 0;
    hours++;
  }
  if (hours == 24) {
    hours = 0;
  }
  textAlign(CENTER);
  textSize(13);
  fill(0);
  if (seconds < 10) {
    text(hours + " : " + minutes + " : 0" + seconds, 70, 30);
  } else {
    text(hours + " : " + minutes + " : " + seconds, 70, 30);
  }
}

